# README #

Django project to get data around the web.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


# Project setup 

## Installing dependencies
Before starting you should install docker and docker-compose on your laptop/PC.

**Installing docker**

You want to install Docker Community Edition: https://store.docker.com/search?type=edition&offering=community.

It's a bit annoying that they require account creation and community was quite angry about that at first. 

**Installing docker-compose**

Follow https://docs.docker.com/compose/install/


**Not mandatory (hint)**

If you are new to django, python dev you should use sth like virtualenv to isolate dependencies from the system python.

I highly recommend doing so.

You can install virtualenv, virtualenvwrapper as describe here.
https://docs.python-guide.org/dev/virtualenvs/

Then you should create dedicate virtualenv for your project with e.g. mkvirtualenv upwork.


## Running examples

### Running hello-world

If you run:
```
 docker run hello-world
```
you will see prompt with explanation.

### Running ubuntu
Run docker hello world:

```
docker run -it ubuntu bash
```

Docker will:

* pull image from public registry from
 `https://hub.docker.com/_/ubuntu/` 
* run it and you will be in bash of that container.


### Run Django (example1)

Basic project is based on: https://semaphoreci.com/community/tutorials/dockerizing-a-python-django-web-application. You should skip docker-machine installation.

```
cd example1
docker build -t example1 . 
docker run -it -p 8000:8000 example1
```

Now you can check running container, by visiting http://localhost:8000.

This setup is using sqlite.

### Run Django - docker compose (example2)

Project is based on https://docs.docker.com/compose/django/#create-a-django-project.



You can recreate it by running:
```
 cd example2
 rm -rf manage.py composeexample
 docker-compose run web django-admin.py startproject composeexample .
 chown -R $USER:$USER .
 ```

 
Per guide you should update example2/composeexample/settings.py
``` 
 DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}
```

 
Now start:

```
docker-compose up
```

You can open http://localhost:8001.

Note it was important to change base python version to 3.6 in Dockerfile due to following error: https://stackoverflow.com/questions/48822571/syntaxerror-generator-expression-must-be-parenthezised-python-manage-py-migra.


### Run drupal (example3)

You can run drupal with db (https://docs.docker.com/samples/library/drupal/#-via-docker-stack-deploy-or-docker-compose):

```
docker-compose up
```

During setup set postgres as db url, username: postgres, password: example.

### Drupal + elastic search 

Basically example3 with elastic search

``` 
  docker-compose up
```

Verify connectivity to elasticsearch:

```
 docker-compose exec  drupal /bin/bash   
 curl http://elasticsearch:9200

```

Note you can access services logs with docker-compose

e.g.  docker-compose logs -f elasticsearch


### How to distribute to AWS.

* Simplest setup: You could just rsync your app and build there and use docker-compose.

* Build on local instance and ship image to target host per instruction https://code.i-harness.com/en/q/16d38a5

* Use AWS registry to host your image and just keep docker-compose.yml on EC2 instance. Install docker, docker-compose and aws cli (https://docs.aws.amazon.com/cli/latest/userguide/installing.html). Then use docker-compose pull to pull and run images from registry.

* if you just run hello world examples which don't have any secrets you could use docker hub (https://hub.docker.com/) instead.



